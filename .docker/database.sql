CREATE SCHEMA IF NOT EXISTS `exads` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `exads` ;

SET foreign_key_checks = 0;
DROP TABLE IF EXISTS  `exads`.`exads_test`;
SET foreign_key_checks = 1;

CREATE TABLE `exads_test` (
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE,
	`name` varchar(255) NOT NULL,
	`age` int(3) NOT NULL,
	`job_title` varchar(255) NOT NULL,
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`))
ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;