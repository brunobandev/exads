#!/bin/bash
.SILENT:
.PHONY: help

include .docker/env
export

## exads | Create and start containers
exads-up:
	@docker-compose -f .docker/docker-compose.yml up -d ${service} --build

## exads | Follow containers logs
exads-logs:
	@docker-compose -f .docker/docker-compose.yml logs -f ${service}

## exads | Install migrations
exads-migration:
	@docker cp .docker/database.sql "${MYSQL_HOST}":/
	@docker exec -it "${MYSQL_HOST}" bash -c "mysql -u ${MYSQL_USERNAME} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} < /database.sql"

## exads | Stop and remove containers
exads-down:
	@docker-compose -f .docker/docker-compose.yml down

## Show this help
help:
	printf "Commands:\n"
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-15s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)