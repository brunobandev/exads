# Exads - Technical Test

## Requirements

```bash
docker
docker-compose
```

## Install

```bash
$ git clone git clone git@bitbucket.org:brunobandev/exads.git exads-bruno
$ cd exads-bruno
```

```bash
$ make exads-up
$ make exads-migration
```

For more comands:

```bash
$ make help
```

Thank you :)