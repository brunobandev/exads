<?php

// Number of elements to be removed.
$numberOfRemovals = 2;

// Creating a array with 500 elements.
$listOfElements = range(1, 500);

// Randomly selecting elements to be removed.
$randomKeysToRemove = array_rand($listOfElements, $numberOfRemovals);

// Removing elements.
if (is_array($randomKeysToRemove)) {
    foreach($randomKeysToRemove as $randomKeyToRemove) {
        unset($listOfElements[$randomKeyToRemove]);
    }
} else {
    unset($listOfElements[$randomKeysToRemove]);
}

// Building a new array with the same number of elements as the original.
$newListOfElements = range(1, max($listOfElements));                                                    

// Getting the missing elements.
$positionsMissing = array_diff($newListOfElements, $listOfElements);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Exads - 2. 500 Element Array</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">2. 500 Element Array</h1>
            <a href="https://bitbucket.org/brunobandev/exads/src/master/public/500.php" target="_blank" class="btn btn-primary mb-2"><strong>BitBucket</strong></a>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Task description</h4>
                <hr>
                <p class="mb-0">Write a PHP script to generate a random array of 500 integers (values of 1 – 500 inclusive). 
                    Randomly remove and discard an arbitary element from this newly generated array.</p>
                <br>
                <p>Write the code to efficiently determine the value of the missing element. Explain your reasoning with comments.</p>
            </div>

            <p class="h2">Result:</p>
            <div class="alert alert-warning" role="alert">
                You can <strong>reload</strong> the page for data update!
            </div>
            <hr>
            <div class="row">
                <div class="col-6">
                    <p class="lead">List with elements removed</p>
                    <ul class="list-group">
                        <?php
                        foreach ($listOfElements as $key => $value) { ?>
                        <li class="list-group-item list-group-item-action"">Key: <strong><?php echo $key; ?></strong>  - Value: <strong><?php echo $value; ?></strong></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-6">
                    <p class="lead">Removed items</p>
                    <ul class="list-group">
                        <?php
                        foreach ($positionsMissing as $key => $value) { ?>
                        <li class="list-group-item list-group-item-action">key: <strong><?php echo $key; ?></strong> - value: <strong><?php echo $value; ?></strong></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </main>
</body>
</html>