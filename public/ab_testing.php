<?php

// Model. It is possible add other records.
$designs = [
    "Design 1" => 50,
    "Design 2" => 25,
    "Design 3" => 25,
];

/**
 * Receive data and return the design based on probability.
 *
 * @param array $designs
 * @return void
 */
function showProbablyDesign(array $designs) {
    // Generate a random number.
    $random = mt_rand(1, (int) array_sum($designs));
    foreach ($designs as $key => $value) {
        // Random number decrements each lap.
        $random = $random - $value;
        // When the random number will be negative, return the design selected.
        if ($random <= 0) {
            return $key;
        }
    }
}

// Example for 100 samplings.
$sampling = 100;
$occurences = [];
for ($i = 0; $i < $sampling; $i++) {
    array_push($occurences, showProbablyDesign($designs));
}

$reports = array_count_values($occurences);
ksort($reports);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Exads - 5. A/B Testing</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">5. A/B Testing</h1>
            <a href="https://bitbucket.org/brunobandev/exads/src/master/public/ab_testing.php" target="_blank" class="btn btn-primary mb-2"><strong>BitBucket</strong></a>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Task description</h4>
                <hr>
                <p class="mb-0">Exads would like to A/B test a number of promotional designs to see which provides the best conversion rate. <br />
                    Write a snippet of PHP code that redirects end users to the different designs based on the database table below. Extend the database model as needed.</p>
                <br>
                <p>i.e. - 50% of people will be shown Design A, 25% shown Design B and 25% shown Design C. 
                    The code needs to be scalable as a single promotion may have upwards of 3 designs to test.</p>
            </div>
            <p class="h2">Result:</p>
            <div class="alert alert-warning" role="alert">
                You can <strong>reload</strong> the page for data update!
            </div>
            <hr>
            <ul class="list-group">
                <?php
                foreach ($reports as $key => $value) { ?>
                <li class="list-group-item list-group-item-action"><strong><?php echo $key; ?></strong> was displayed <strong><?php echo $value; ?></strong> times for a total of <strong><?php echo $sampling; ?></strong><br /> </li>
                <?php } ?>
            </ul>
        </div>
    </main>
</body>
</html>
