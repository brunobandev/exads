<?php

class Exads {

    protected $host = "exads-db";
    protected $user = "root";
    protected $pass = "secret";
    protected $base = "exads";
    protected $connection = null;

    /**
     * Constructor.
     */
    public function __construct() {
    }

    /**
     * Desconect after executions.
     */
    public function __destruct()
    {
        if ($this->connection != null) {
            $this->disconnect();
        }
    }

    /**
     * Connect with database.
     *
     * @return void
     */
    public function connect() {
        $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->base);

        if ($this->connection->connect_error) {
            echo "Error: Unable to connect to MySQL." . "<br />";
            echo "Debugging errno: " . mysqli_connect_errno() . "<br />";
            echo "Debugging error: " . mysqli_connect_error() . "<br />";
            exit;
        }
    }

    /**
     * Execute query in database.
     *
     * @param string $query
     * @param array $params
     * @return void
     */
    public function execute(string $query, array $params = null) {
        $this->connect();
        
        if ($params != null) {
            $queryParts = preg_split("/\?/", $query);
            
            if (count($queryParts) != count($params) + 1) {
                return false;
            }
            $finalQuery = $queryParts[0];
            for ($i = 0; $i < count($params); $i++) {
                $finalQuery = $finalQuery . $this->sanitise($params[$i]) . $queryParts[$i + 1];
            }

            $query = $finalQuery;
        }

        $result = $this->connection->query($query);
        return $result;
    }

    /**
     * Disconnect.
     *
     * @return void
     */
    public function disconnect() {
        if ($this->connection != null) {
            $this->connection->close();
            $this->connection = null;
        }
    }

    /**
     * Sanitise against injection.
     *
     * @param mixed $parameters
     * @return mixed
     */
    protected function sanitise($parameters) {
        $result = $this->connection->real_escape_string($parameters);
        return $result;
    }
}

// New instance.
$exads = new Exads();

// Return all records.
$queryList = "SELECT id, name, age, job_title FROM exads_test";
$resultList = $exads->execute($queryList);

// New record by form.
if (isset($_POST) && !empty($_POST)) {
    $dataInsert = [
        $_POST['name'], 
        $_POST['age'], 
        $_POST['job_title'],
    ];

    $queryInsert = "INSERT INTO `exads_test` (`name`, `age`, `job_title`) VALUES ('?', '?', '?')";
    $resultInsert = $exads->execute($queryInsert, $dataInsert);
    header("Location: database_connectivity.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Exads - 3. Database Connectivity</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">3. Database Connectivity</h1>
            <a href="https://bitbucket.org/brunobandev/exads/src/master/public/database_connectivity.php" target="_blank" class="btn btn-primary mb-2"><strong>BitBucket</strong></a>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Task description</h4>
                <hr>
                <p class="mb-0">Demonstrate with PHP how you would connect to a MySQL (InnoDB) database and query for all 
                    records with the following fields: (name, age, job_title) from a table called 'exads_test'.</p>
                <br>
                <p>Also provide an example of how you would write a sanitised record to the same table.</p>
            </div>
            <p class="h2">Result:</p>
            <hr>
            <form action="" method="post">
                <div class="form-row">
                    <div class="col-6">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" required>
                    </div>
                    <div class="col">
                        <input type="text" name="age" id="age" class="form-control" placeholder="Age" required>
                    </div>
                    <div class="col-4">
                        <input type="text" name="job_title" id="job_title" class="form-control" placeholder="Job Title" required>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </form>
            <?php
            if ($resultList->num_rows > 0) { ?>      
            <p class="mt-4">Number of records: <strong><?php echo $resultList->num_rows; ?></strong></p>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Age</th>
                        <th scope="col">Job</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row = $resultList->fetch_object()) { ?>
                    <tr>
                        <th scope="row"><?php echo $row->id; ?></th>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->age; ?></td>
                        <td><?php echo $row->job_title; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } else { ?>
                <div class="alert alert-primary mt-3" role="alert">
                    <strong>No records.</strong>
                </div>
            <?php } ?>
        </div>
    </main>
</body>
</html>
