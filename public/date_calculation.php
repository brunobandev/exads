<?php

date_default_timezone_set("Europe/London");

class ExadsLottery {
    
    // Config.
    protected $startDate;
    protected $hourBase = "20:00:00";
    protected $daysOfWeek = [
        'wednesday',
        'saturday',
    ];
    
    /**
     * Set the start date to calculate the next dates.
     *
     * @param string $startDate
     */
    public function __construct($startDate = null) {
        $this->startDate = (isset($startDate)) ? new DateTime($startDate) : new DateTime("now");
    }
    
    /**
     * Get the next lottery dates.
     *
     * @return array
     */
    public function lotteryNextDates() {
        
        $nowDate = (new DateTime('now'))->format('Y-m-d');
        $inputDateTime = $this->startDate->format('Y-m-d H:i:s');
        $inputDate = $this->startDate->format('Y-m-d');
        $inputTime = $this->startDate->format('H:i:s');
        $timestampDays = [];
        
        // Read available days.
        foreach ($this->daysOfWeek as $dayOfWeek) { 
            $newToday = new DateTime($inputDateTime);
            
            // Is it today? Check hour as well. If have lottery today put inside the array.
            // Else put the next date.
            if (($nowDate === $inputDate) && ($inputTime < $this->hourBase)) {
                $auxNextDay = $newToday->modify("$dayOfWeek $this->hourBase");
            } else {
                $auxNextDay = $newToday->modify("next $dayOfWeek $this->hourBase");   
            }
            
            // Insert in array.
            array_push($timestampDays, $auxNextDay->format('Y-m-d H:i:s'));
        }
        
        asort($timestampDays);
        return $timestampDays;
    }
}

// Example. Receive new date.
if (isset($_POST['dateInput']) && !empty($_POST['dateInput'])) {
    $dateInput = $_POST['dateInput'];
}

// New instance.
$lotteryDates = (new ExadsLottery($dateInput))->lotteryNextDates();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" />
    <title>Exads - 4. Date Calculation</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">4. Date Calculation</h1>
            <a href="https://bitbucket.org/brunobandev/exads/src/master/public/date_calculation.php" target="_blank" class="btn btn-primary mb-2"><strong>BitBucket</strong></a>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Task description</h4>
                <hr>
                <p class="mb-0">The Irish National Lottery draw takes place twice weekly on a Wednesday and a Saturday at 8pm.</p>
                <br>
                <p>Write a function or class that calculates and returns the next valid draw date based on the current 
                    date and time AND also on an optionally supplied date and time.</p>
            </div>
            <p class="h2">Result:</p>
            <hr>
            <form action="" method="post">
                <div class="form-row">
                    <div class="col-6">
                        <input type="text" name="dateInput" id="datetimepicker_mask" class="form-control">
                    </div>
                    <div class="col-1">
                        <button class="btn btn-primary" type="submit">Search</button>
                    </div>
                </div>
            </form>
            <ul class="list-group mt-3">
                <?php
                foreach ($lotteryDates as $lotteryDate) { ?>
                <li class="list-group-item list-group-item-action"><?php echo "Next Irish National Lottery: " . (new DateTime($lotteryDate))->format("l, d/m/Y, h:i A"); ?> </li>
                <?php } ?>
            </ul>
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
    <script>
        jQuery('#datetimepicker_mask').datetimepicker({
            timepicker: true,
            mask: false,
        });
    </script>
</body>
</html>

