<?php

// Array to check. It is possible to add other numbers.
$multipleOf = [
    3 => 'Fizz',
    5 => 'Buzz',
];

/**
 * Checks if the number is multiple, 
 * and returns the stipulated value or itself.
 *
 * @param integer $i
 * @param array $multipleOf
 * @return mixed
 */
function checkNumberIsMultipleOf(int $i, array $multipleOf) {
    
    $isMultipleOf = false;
    // Sort the array by keys
    ksort($multipleOf);
    $result = "";

    // Check the number array
    foreach ($multipleOf as $key => $value) {
        // Verify if is multiple by key.
        if ($i % $key == 0) {
            $isMultipleOf = true;
            $result = $result . $value;
        }
    }

    // If It is multiple by key, return the new result, else the number verified.
    return ($isMultipleOf) ? $result : $i;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Exads - 1. FizzBuzz</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">1. FizzBuzz</h1>
            <a href="https://bitbucket.org/brunobandev/exads/src/master/public/fizzbuzz.php" target="_blank" class="btn btn-primary mb-2"><strong>BitBucket</strong></a>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Task description</h4>
                <hr>
                <p class="mb-0">Write a PHP script that prints all integer values from 1 to 100.</p>
                <br>
                <p>For multiples of three output "Fizz" instead of the value and for the multiples of five output "Buzz". 
                    Values which are multiples of both three and five should output as "FizzBuzz".</p>
            </div>
            <p class="h2">Result:</p>
            <hr>
            <ul class="list-group">
                <?php
                for ($i = 1; $i <= 100; $i++) { ?>
                <li class="list-group-item list-group-item-action"><?php echo checkNumberIsMultipleOf($i, $multipleOf); ?></li>
                <?php } ?>
            </ul>
        </div>
    </main>
</body>
</html>

