<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Welcome - EXADS - Bruno Coronas Bandeira</title>
</head>
<body class="d-flex flex-column h-100">
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php">EXADS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="fizzbuzz.php">1. FizzBuzz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="500.php">2. 500 Element Array</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="database_connectivity.php">3. Database Connectivity</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="date_calculation.php">4. Date Calculation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ab_testing.php">5. A/B Testing</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main role="main" class="flex-shrink-0 mt-5">
        <div class="container">
            <h1 class="mt-5">Welcome!</h1>
            <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Technical Test</h4>
                <hr>
                <ul>
                    <li>All tasks are available in the menu above;</li>
                    <li>In each item, you will find the result and a link to view the source code;</li>
                    <li>If you want to install or view all files, click <a href="https://bitbucket.org/brunobandev/exads/src/master/" target="_blank">here</a> to view the entire repository;</li>
                    <li>Any questions, please contact me;</li>
                </ul>
                <p>
                    <strong>Name:</strong> Bruno Coronas Bandeira<br />
                    <strong>E-mail:</strong> brunoban.dev@gmail.com
                </p>
                <p class="lead">Thank you very much for your time.</p>
            </div>
        </div>
    </main>
</body>
</html>

